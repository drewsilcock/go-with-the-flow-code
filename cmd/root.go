package cmd

import (
	"log"

	"github.com/joho/godotenv"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	environment string

	rootCmd = &cobra.Command{
		Use:   "hbaas-server",
		Short: "HBaaS server main command",
		Long:  "Backend server for Happy Birthday as a Service (HBaaS) demo RESTful for birthday greetings.",
		Run:   runServer,
	}
)

func Execute() error {
	return rootCmd.Execute()
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVarP(
		&environment,
		"env",
		"e",
		"dev",
		"Environment to run server in. Either 'dev' or 'prod'",
	)
	if err := viper.BindPFlag(
		"env",
		rootCmd.PersistentFlags().Lookup("env"),
	); err != nil {
		log.Fatal(err)
	}
}

func initConfig() {
	// If this fails, that's fine! We don't need a .env file.
	_ = godotenv.Load()

	viper.SetEnvPrefix("hbaas")
	viper.AutomaticEnv()
}
